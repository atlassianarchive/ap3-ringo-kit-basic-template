# Ringo-Kit Plugin in Delicious CoffeeScript
# ------------------------------------------
# If you've ever played with Sinatra (Ruby), Flask (Python),
# or other micro web frameworks before, the following
# will look pretty familiar. If not, hang in there... it's
# not that hard.
#
# For more on what you can do here, checkout
# https://developer.atlassian.com/static/ap3/ringo-kit/doc/

# Bootstraps the plugin's router
app = exports.app = require("atlassian/app").create()

# Underscore baby!
_ = require("vendor/underscore")

app.configure
  aui: "5.0"
  stylesheets: ["app"]
  scripts: ["app"]
  clientOptions:
    resize: "auto"

app.get "/index", (req, res) ->

  # Sort out query params so we can pass them into our view
  queryParams = _.map req.query, (v, k) ->
    key: k
    value: v

  # Render the view passing a JSON object into it
  # Views are written with the http://handlebarsjs.com
  # templating language
  res.render "index",
    greeting: "Congratulations"
    query: queryParams