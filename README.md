# My Ringo-Kit Plugin

Congratulations... you've created a Ringo-Kit plugin!

To get started, check the [Ringo-Kit documentation](https://developer.atlassian.com/static/ap3/ringo-kit/doc/)).

## JavaScript or CoffeeScript?

This plugin has code for both JavaScript and [CoffeeScript](http://coffeescript.org). Find the `main.js` and `main.coffee` files in the root directory of this plugin. If you plan on writing your plugin with JavaScript, go ahead and delete the `main.coffee`. Likewise, if you plan on using CoffeeScript, delete the `main.js` file.